using UnityEngine;

public class Projectile : MonoBehaviour {
    [HideInInspector] public SpriteRenderer sprite;
    private CapsuleCollider2D collide;
    [HideInInspector] public Rigidbody2D physics;

    public float speed = 50.0f;
    private float mass = 0.005f;
    public float gravity = 0;

    public void Start() {
        sprite = GetComponent<SpriteRenderer>();

        collide = GetComponent<CapsuleCollider2D>();
        physics = GetComponent<Rigidbody2D>();

        physics.mass = mass;
        physics.gravityScale = gravity;
    }

    public void Update() {

    }

    public GameObject create(Transform trans, float rotZ, SpriteRenderer sprite) {
        Debug.Log(sprite.bounds.size.ToString("G"));

        return (GameObject)Instantiate(gameObject, sprite.bounds.center, Quaternion.identity);
    }
    
    public void OnCollisionEnter2D(Collision2D other) {
        print("Hit " + other.gameObject.name);
        Destroy(gameObject);

        if (other.gameObject.name == "Bot") {
            BotHealth healthbar = other.gameObject.GetComponent<BotHealth>();
            healthbar.takeDamage(20);
        } else if (other.gameObject.name == "Player") {
            PlayerHealth healthbar = other.gameObject.GetComponent<PlayerHealth>();
            healthbar.takeDamage(20);
        }
    }
}