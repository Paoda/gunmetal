﻿using System.Collections.Generic;
using UnityEngine;


public class Line : MonoBehaviour {
    private Vector3 mousePos;
    public GameObject projectile;
    private int offScreenBuffer = 10;
    private List<GameObject> gameObjectList = new List<GameObject>();
    private List<Data> dataList = new List<Data>();
    private float bulletSpeed;
    private int maxShots = 6;
    private int numOfShots = 0;
    [HideInInspector] public float angleNormalizer = 31.386f;  //31.386f is a constant of at which point the bullet must fly straight, no angle.

    public void Start() {
        transform.eulerAngles = new Vector3(0,0,0);
    }

    public float getRotation(string type) {
        mousePos = Input.mousePosition;
        Vector3 coords = Camera.main.WorldToScreenPoint(transform.position);

        float angle =  Mathf.Atan((mousePos.y - coords.y) / (mousePos.x - coords.x)); 

        if (type.Equals("rad")) return angle;
        else return toDegrees(angle);
    }
    public virtual void Update() {
        //Rotation
        float rotZRad = getRotation("rad");
        float rotZ = toDegrees(rotZRad) + angleNormalizer;
        Vector3 updatedVector = new Vector3(0, 0, rotZ);

        if (!(float.IsNaN(rotZ))) {
            //If this if statement isn't here things tend to break. damn user input >:(
            transform.eulerAngles = updatedVector;
        }

        if (Input.GetButtonDown("Fire1")) {

            if (numOfShots < maxShots) {
                numOfShots++;
                GameObject bullet = createBullet(rotZ);

                SpriteRenderer sprite = bullet.GetComponent<SpriteRenderer>();

                dataList.Add(new Data());
                gameObjectList.Add(bullet);

                if (sprite) {
                    sprite.sortingOrder = 0;
                    sprite.sortingLayerName = "Foreground";
                }
            } else Debug.Log("Already Shot " + maxShots.ToString("G") + " Times!");
        }

        for (int i = 0; i < gameObjectList.Count; i++) {
            GameObject bullet = gameObjectList[i];
            Data current = dataList[i];

            if (bullet != null) {
                Rigidbody2D physics = gameObjectList[i].GetComponent<Rigidbody2D>();

                calcBulletVelocity(current, bullet, physics, rotZ);
            }
            else {
                //Object was Destroyed before it could go offscreen.
                gameObjectList.Remove(bullet);
                dataList.Remove(current);
            }
        }
    }
    private float toDegrees(float degrees) {
        return (180 / Mathf.PI) * degrees;
    }
    public void onCollisionEnter2D(Collision2D other) {
        print("Hit " + other.gameObject.name);
    }
    public class Data { //Used for Tracking whether the angle of a projectile has been set already.
        public bool isAffected;
        public Vector3 oldVector;

        public Data(bool affected, Vector3 vector) {
            isAffected = affected;
            oldVector = vector;
        }
        public Data() {
            //Defaults
            isAffected = false;
            oldVector = new Vector3(0, 0, 0);
        }
    }
    public GameObject createBullet(float rotation) {
        Projectile bullet = projectile.GetComponent<Projectile>();
        this.bulletSpeed = bullet.speed;

        GameObject instance = bullet.create(transform, rotation, GetComponent<SpriteRenderer>());
        instance.transform.Rotate(new Vector3(0, 0, rotation - angleNormalizer));

        return instance;        
    }

    public void calcBulletVelocity(Data current, GameObject bullet, Rigidbody2D physics, float rotation) {
        Vector3 angle;

        if (!current.isAffected) {
            angle = Quaternion.AngleAxis((rotation - angleNormalizer), new Vector3(0, 0, 1)) * new Vector3(10, 0, 0);
            current.oldVector = angle;
            current.isAffected = true;
        } else angle = current.oldVector;

        physics.velocity = (angle * Time.deltaTime * bulletSpeed);

        Vector3 bulletScreenPos = Camera.main.WorldToScreenPoint(bullet.transform.position);
        if (bulletScreenPos.y > (Screen.height + offScreenBuffer) || bulletScreenPos.y < (0 - offScreenBuffer)) {
            Destroy(bullet);
            gameObjectList.Remove(bullet);
            dataList.Remove(current);
        }
        
        if (bulletScreenPos.x > (Screen.width + offScreenBuffer) || bulletScreenPos.x < (0 - offScreenBuffer)) {
            Destroy(bullet);
            gameObjectList.Remove(bullet);
            dataList.Remove(current);
        }

    }
}

