﻿using UnityEngine;

public class Stickman : MonoBehaviour {
	private SpriteRenderer sprite;
	public float length;
	public float height;
	private PlayerHealth healthbar;
	public virtual void Start () {
		//Get Boundaries of Object
		sprite = GetComponent<SpriteRenderer>();
		length = sprite.bounds.size.x;
		height = sprite.bounds.size.y;
		print(gameObject.name + " Length: " + length.ToString("G") + " Height: " + height.ToString("G"));
		
		healthbar = GetComponent<PlayerHealth>();

	}

	// Update is called once per frame
	public virtual void Update () {

	}
}
