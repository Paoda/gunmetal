﻿using System.Collections;
using UnityEngine;

public class Score {
	
	private static int[,] arr = new int[6, 1];
	private static int round = 0;
	
	public static void addScore(int victor) {
		//1 = Player
		// 2 = Bot
		// 3 = Tie
		round++;
		if (round < 6) {
			if (victor > 0 && victor < 4) arr[round, 0] = victor;
			else Debug.Log("ID: " + victor.ToString("G") + "Is not a compatible winner integer");
		} else Debug.Log("6 Rounds Completed.");
	}
	public static int[,] getScores() {
		return arr;
	}

	private static int[,] getLeader() {
		int[,] res = new int[1, 2];
		int player = 0, tie = 0, bot = 0;

		for(int i = 0; i < arr.GetLength(0); i++) {
			for (int j = 0; j < arr.GetLength(1); j++) {
				if (arr[i, j] == 1) player++;
				else if (arr[i, j] == 2) bot++;
				else tie++;
			}
		}

		//Probably should account for Tie edge case. 
		if (player > bot) {
			res[0, 0] = 1;
			res[0, 1] = player;
			return res;
		} else if (bot > player) {
			res[0, 0] = 2;
			res[0, 1] = bot;
			return res;
		} else {
			res[0, 0] = 3;
			res[0, 1] = player; //Or bot is fine, theyr'e the same.
			return res;
		}
	}
	public static bool roundsLeft() {
		if (round < 6) {
			int[,] winner = getLeader();

			if (winner[0, 0] == 3) {// Is tie
				if (winner[0, 1] >= 3) return false; // 3L 3W (Tie)
				else return true;
			}
			else if (winner[0, 1] > 3) return false;
			else return true;
		} else return false;
	}

	public static void reset() {
		round = 0;
		arr = new int[6, 1];
		Debug.Log("Scores Resetted!");
	}
}