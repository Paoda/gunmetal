﻿using System.Timers;
using UnityEngine;

public class Countdown {
	private Timer timer = new Timer();
	private bool isEnabled = false;
	private int totalTime; //seconds
	public Countdown(int time) {
		totalTime = time;
		timer.Elapsed += new ElapsedEventHandler(onTimedEvent);
		timer.Interval = 1000; //miliseconds
		timer.Enabled = isEnabled; //Defaults to False.
	}
	public void start() {
		isEnabled = true;
		timer.Enabled = isEnabled;
	}
	public void stop() {
		isEnabled = false;
		timer.Enabled = isEnabled;
	}
	private void onTimedEvent(object src, ElapsedEventArgs e) {
		if (totalTime > 0) totalTime--;
		else done();
		// Debug.Log(this.ToString());
	}
	private void done() {
		stop();
		Debug.Log("Completed");
	}
	public override string ToString() {
		int minutes = Mathf.FloorToInt((float)totalTime / 60);
		int seconds = (totalTime % 60) % 60;
		string res = minutes.ToString("G") + ":";

		if (seconds < 10) res += "0" + seconds.ToString("G");
		else res += seconds.ToString("G");

		return res;
	}
	public int getTimeLeft() {
		return totalTime;
	}
}
