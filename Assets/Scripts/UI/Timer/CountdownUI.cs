using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CountdownUI : MonoBehaviour {
    public Text text;
    private string defaultStr = "6:00";
    private Countdown ctdn;
    public void Start() {
        text.text = defaultStr;
        ctdn = new Countdown(360);
        ctdn.start();

    }

    public void Update() {
        text.text = ctdn.ToString();
    }

}
