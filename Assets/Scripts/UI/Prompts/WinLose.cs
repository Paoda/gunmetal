using UnityEngine;

public class WinLose : MonoBehaviour {
    public GameObject win;
    public GameObject lose;

    public void Start() {
        win.SetActive(false);
        win.SetActive(false);
    }

    public void hide(int id) {
        if (id == 1) win.SetActive(false);
        else if (id == 2) lose.SetActive(false);
        //Else Hide Tie
    }
    public void show(int id) {
        if (id == 1) win.SetActive(true);
        else if (id == 2) lose.SetActive(true);
        //Else Show Tie
    }
}