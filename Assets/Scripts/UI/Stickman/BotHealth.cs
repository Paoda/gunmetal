using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BotHealth : Health {
    public Slider healthbar;

    public override void Start() {
        healthbar.value = getFloatHealth();
    }

    public override void takeDamage(int dmg) {
        if (!dead) {
            health -= dmg;
            if(health <= 0) die(2);
            healthbar.value = getFloatHealth();
        }
    }
}