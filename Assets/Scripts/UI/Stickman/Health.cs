using UnityEngine;
using UnityEngine.SceneManagement;

public class Health : MonoBehaviour {
    [HideInInspector] public int health = 100;
    [HideInInspector] public bool dead = false;
    public GameObject win;
    public GameObject lose;
    private WinLose results;

    public virtual void Start() {
        Debug.Log("Healthbar: " + gameObject.name);
        results = GetComponent<WinLose>();
    }
    public virtual void Update() {

    }
    public bool isDead() {
        return dead;
    }

    public void die(int id) {
        dead = true;
        Debug.Log("Someone has Died!");
        Score.addScore(id);
        
        if (Score.roundsLeft()) {
            SceneManager.LoadScene("Main Game");
        } else getWinner(id);
    }

    public virtual void getWinner(int id) {
        Debug.Log("Winner ID: " + id.ToString("G"));
        results.show(id);
    }

    public float getFloatHealth() {
        return (float)health / 100;
    }
    public virtual void takeDamage(int dmg) {
        health -= dmg;
        if (health <= 0) dead = true;
    }
}