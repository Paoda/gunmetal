using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PlayerHealth : Health {
    public Slider healthbar;

    public override void Start() {
        healthbar.value = getFloatHealth();
        healthbar.direction = Slider.Direction.RightToLeft;
    }

    public override void takeDamage(int dmg) {
        if (!dead) {
            health -= dmg;
            if(health <= 0) die(1);
            healthbar.value = getFloatHealth();
        }
    }
}